# Slotcar Timing

This Repo ties all the parts together and will be used to run the entire system in the end.

## Components

The Backend lives in [the backend repo](https://gitlab.com/slotcar-timing/backend)

Frontends will get their own repositories as well... At some point. The Fontend in here is a very simple demo

The Software for the timing client will live in another repo that doesn't exist as of now.

## Set Up this environment

* Clone this repo (duh)
* Create a [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens) That has the API scope
* In your console, run `docker login registry.gitlab.com`. Use your PAT from Step 2 as password
* `docker network create slotcar-net` to create the network used by this docker-compose file
* `docker-compose up -d` should build/pull everything you need
* `yarn install` to get the dependencies for the laptime generator
* There are a couple of helpers that can be launched from the `helpers/` directory
  - `laptime_dummy.js` generates laptimes
  - `socketio-test.js` is a SocketIO client that logs received messages to the console


## Developing one of the components

The entire repo group is meant to work together.
This repo provides the entire stack of applications.

Every component repository should provide a docker-compose.yml that can act as a drop-in replacement
for the service listed in this docker-compose file.

To start development work on a component:
- `docker-compose down` in this (infrastructure) repo
- Comment out the service you want to develop in this repo's `docker-compose.yml`
- Clone the component repository & cd into it
- `docker-compose up -d` in the component repository
- `docker-compose up -d` in the infrastructure repository

## Building a new Frontend

* Build something that can understand the endpoints of the backend
* Containerize it
* Have a look at the [Frontend Example](https://gitlab.com/slotcar-timing/frontend/example)
  - Build a docker-compose.yml as shown there (Take a look at the network stuff!)
  - Maybe push the container somewhere other people can find it
  - Create a section for your new frontend in the `docker-compose.yml` in this repo
  - In the `nginx.conf`, Copy&Adapt the settings from another frontend

## Development/Testing

- [MQTT Explorer](https://mqtt-explorer.com) is a nice frontend to send & inspect MQTT messages
