const mqtt = require('mqtt')
const yargs = require('yargs')
const { hideBin } = require('yargs/helpers')

options = yargs(hideBin(process.argv))
  .command("$0", 'Send messages to [host]', function (yargs) {
    yargs.positional('broker', {
      describe: 'MQTT Broker hostname',
      type: "String",
      default: "localhost"
    })
  })
  .option("a", {
    alias: "average_time",
    describe: "Average laptime (seconds)",
    type: "number",
    default: 5,
    demandOption: false
  })
  .option("d", {
    alias: "deviation",
    describe: "Maximum deviation of laptimes from average (seconds)",
    type: "number",
    default: 4,
    demandOption: false
  }).help().parse()

function getLaptime() {
  let averageMillis = options.average_time * 1000
  let deviationMillis = options.deviation * 1000
  deviationMillis = (Math.random() * (2 * deviationMillis)) - deviationMillis

  let timeoutMillis = averageMillis + deviationMillis

  return Math.round(Math.max(0, timeoutMillis))
}

let startTime = new Date().getTime()
function getTimestamp() {
  return new Date().getTime() - startTime
}



const client = mqtt.connect(`mqtt://${options.broker}`)

function loopMe(laneNumber, sequence = 0) {
  setTimeout(function () {
    msg = JSON.stringify({ sequence: sequence, timestamp: getTimestamp() })
    client.publish(
      `slotcars/beam_broken/${laneNumber}`,
      msg
    )
    console.log(`sent MSG: ${msg}`)
    loopMe(laneNumber, ++sequence)
  },
    getLaptime()
  )
}

client.on('connect', function () {
  console.log(`connected to ${options.broker}`)
  loopMe(1)
  loopMe(2)
})
