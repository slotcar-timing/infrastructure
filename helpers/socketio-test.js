const io = require('socket.io-client')

const socket = io('http://localhost', {path: '/api/socket.io'})

socket.on("connect", () => {
    console.log("Connected to server!")
})

socket.on("laps_completed", (data) => {
    console.log(`Laps completed: ${data}`)
})
